import React, { Component } from 'react';
import Cell from './Cell';


class Frame extends Component{

    constructor(props){
        super(props);
        this.state = {
            cells: Array(9).fill(null),
            turn : 'X'
        };
    }

    clickOnCell = (indexCell) => {

        //Je vérifie que la cellule sur laquelle je clique est vide
        //S'il elle ne l'est pas (!==null), alors je quitte la fonction ...
        // ... pour ne pas exécuter la suite du code permettant la maj du state
        if(this.state.cells[indexCell] !== null)
            return;

        //Je récupère le tableau des cellules du state pour le modifier
        let newCells = this.state.cells;
        //Je mets à jour l'index de la cellule cliquée avec le bon joueur
        newCells[indexCell] = this.state.turn;

        /* Changer de tour avec un if normal
            let newTurn = '';
            if(this.state.turn == 'X')
                newTurn = 'O'
            else
                newTurn = 'X'
         */

        //Je mets à jour mon state pour raffraichir mon affichage
        this.setState({
            cells: newCells, //Les cellules maj
            turn: (this.state.turn == 'X')? 'O' : 'X', //Je change le tour
        })
    }


    displayCells = () => {
        return this.state.cells.map(
            (cell, i) => {
                return <Cell val={cell} clickOnCell={() => this.clickOnCell(i)}/>
            }
        )
    }


    render(){
        return (
            <div id="frame">
                {this.displayCells()}
            </div>
        );
    }

}

export default Frame;